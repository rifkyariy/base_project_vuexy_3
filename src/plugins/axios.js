import axios from 'axios'

const baseURL = `${process.env.VUE_APP_BASE_API_URL}`

const axiosIns = axios.create({
// You can add your headers here
// ================================
  baseURL,
  timeout: 10000,

// headers: {'X-Custom-Header': 'foobar'}
})

export default axiosIns
